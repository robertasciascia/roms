% Script to compare MFS and Satellite data 

addpath(genpath('/mnt/data2/sciascia/utilities/'))

clear all; close all;


yr = 2004 ;
yrstr = num2str(yr);

% Read MFS v4b3

for i=1:12
    if i<10
       mth=['0',num2str(i)];
    else
       mth=num2str(i);
    end

    MFSdir = '/mnt/data2/ROMS/mfs/newSshDec15/';
    sname = ['Mfs_Ssh_',mth,'_',yrstr,'.nc'];
    sshFile = [MFSdir,sname];
    ssh_temp=ncread(sshFile,'sossheig');
    if i==1
       MFSssh=ssh_temp;
    else
      MFSssh=cat(3,MFSssh,ssh_temp);
    end
end

Mlat=double(ncread(sshFile,'lat'));
Mlon=double(ncread(sshFile,'lon'));
[MFSlon,MFSlat]=meshgrid(Mlon,Mlat);

avrg_MFSssh=nanmean(MFSssh(:));
MFSssh=MFSssh-avrg_MFSssh;


% Read MFS v4a3

for i=1:12
    if i<10
       mth=['0',num2str(i)];
    else
       mth=num2str(i);
    end

    MFSdir = '/mnt/data2/ROMS/mfs/Ssh/';
    sname = ['Mfs_Ssh_',mth,'_',yrstr,'.nc'];
    sshFile = [MFSdir,sname];
    ssh_temp=ncread(sshFile,'sossheig');
    if i==1
       MFSssh_old=ssh_temp;
    else
      MFSssh_old=cat(3,MFSssh_old,ssh_temp);
    end
end

time=[1:1:size(MFSssh_old,3)];


avrg_MFSssh_old=nanmean(MFSssh_old(:));
MFSssh_old=MFSssh_old-avrg_MFSssh_old;



% Read SATELLITE SLA 


SATdir = '/mnt/data2/ROMS/SatData/CopernicusSatData/MergedSat/';
sname = ['AllSat_Merged_MeanSeaLevel_L4_REP_OBSERVATIONS_',yrstr,'.nc'];
slaFile = [SATdir,sname];
Slat=double(ncread(slaFile,'lat'));
Slon=double(ncread(slaFile,'lon'));
Slon=rem((Slon+180),360)-180;

[SATlon,SATlat]=meshgrid(Slon,Slat);

sla_temp=ncread(slaFile,'sla');
SATsla=sla_temp;
SATsla=SATsla(:,:,1:end-1);


% Read SATELLITE MDT

MDTdir = '/mnt/data2/ROMS/SatData/AvisoSatData/MeanDynTopo/';
mname = ['SMDT-MED-2014-REF20.nc'];
mdtFile = [MDTdir,mname];
Mlat=double(ncread(mdtFile,'lat'));
Mlon=double(ncread(mdtFile,'lon'));
Mlon=rem((Mlon+180),360)-180;
[MDTlon,MDTlat]=meshgrid(Mlon,Mlat);

mdt=ncread(mdtFile,'mdt')';
 

EtaMin = -0.25;
EtaMax = 0.25;

seaMFS = ~isnan(MFSssh(:,:,1)');
seaSAT = ~isnan(SATsla(:,:,1)');
seaMDT = ~isnan(mdt(:,:,1));

tempInt = NaN*SATlon;
tempInt_old = NaN*SATlon;

MFSint = NaN*ones(128,340,time(end));
MFSint_old = NaN*ones(128,340,time(end));


mdtInt = NaN*SATlon;
mdtInt(seaSAT) = griddata(MDTlon(seaMDT),MDTlat(seaMDT),mdt(seaMDT),SATlon(seaSAT),SATlat(seaSAT),'nearest');




for i=1:time(end)
temp=squeeze(MFSssh(:,:,i))';
temp_old=squeeze(MFSssh_old(:,:,i))';
tempInt(seaSAT) = griddata(MFSlon(seaMFS),MFSlat(seaMFS),temp(seaMFS),SATlon(seaSAT),SATlat(seaSAT),'nearest');
tempInt_old(seaSAT) = griddata(MFSlon(seaMFS),MFSlat(seaMFS),temp_old(seaMFS),SATlon(seaSAT),SATlat(seaSAT),'nearest');
MFSint(:,:,i)=tempInt;
MFSint_old(:,:,i)=tempInt_old;
SATssh(:,:,i)=SATsla(:,:,i)' + mdtInt;
diff(:,:,i)=SATssh(:,:,i)  - MFSint(:,:,i);
diff_old(:,:,i)=SATssh(:,:,i)  - MFSint_old(:,:,i);
end


filename=['/mnt/data2/sciascia/ConnectivitySimulations/InputData/diffMFSSat_',yrstr,'.mat'];
save(filename, 'diff','diff_old');


MFSint_2004=squeeze(nanmean(MFSint,3));
MFSintold_2004=squeeze(nanmean(MFSint_old,3));
SATssh_2004=squeeze(nanmean(SATssh,3));



% ROMS MODEL 

nt=1465;

for i = 1:nt
    if i>=1000, blank = ''; end
    if i<1000, blank = '0'; end
    if i<100,  blank = '00'; end
    if i<10,   blank = '000'; end
    sname = ['/mnt/iscsi/ROMS/lig1kmAuto/Exp4/Out_data/ocean_his_',blank,num2str(i),'.nc'];

    sshFile = [sname];
    ROMSlat=ncread(sshFile,'lat_rho');
    ROMSlon=ncread(sshFile,'lon_rho');
    ssh_temp=squeeze(ncread(sshFile,'zeta'));
    ROMSssh(:,:,i)=ssh_temp;

end
avrg_ROMSssh=nanmean(ROMSssh(:));
ROMSssh= ROMSssh-avrg_ROMSssh;
ROMSssh_2004=squeeze(nanmean(ROMSssh,3));




fig1=figure(1);
set(gcf,'Position', [165 337 1120 387],'InvertHardcopy','on','PaperPositionMode','auto')
plot(time,squeeze(nanmean(nanmean(MFSssh,1),2)),'r');hold on;
plot(time,squeeze(nanmean(nanmean(MFSssh_old,1),2)),'b');
plot(time,squeeze(nanmean(nanmean((SATssh),1),2)),'k');
hold off
h=legend('MFS - 4b3','MFS - 4a3','SAT');
set(h,'Location','SouthEast')
set(gca,'FontSize',14)
axis([0 size(time,2) -0.3 0.2])
datetick('x','mmm')
xlabel(yrstr)
ylabel('SSH [m]')
exportfile = ['/mnt/data2/sciascia/ConnectivitySimulations/InputData/SSH_MFSSat_',yrstr,'.png'];
print(fig1,'-dpng', exportfile)


fig2=figure(2);
set(gcf,'Position', [141 147 1083 558],'InvertHardcopy','off','PaperPositionMode','auto')
pcolor(SATlon,SATlat,MFSint_2004); shading interp; caxis([EtaMin EtaMax]); colorbar;colormap(bluewhitered)
xlabel('Lon')
ylabel('Lat')
title('MFS-4b3 SSH field interpolated on SAT grid')
set(gca, 'Fontsize',14, 'Color',[0.5 0.5 0.5])
exportfile = ['/mnt/data2/sciascia/ConnectivitySimulations/InputData/MFS_v4b3_map',yrstr,'.png'];
print(fig2,'-dpng', exportfile)


fig3=figure(3);
set(gcf,'Position', [141 147 1083 558],'InvertHardcopy','off','PaperPositionMode','auto')
pcolor(SATlon,SATlat,MFSintold_2004); shading interp; caxis([EtaMin EtaMax]); colorbar;colormap(bluewhitered)
axis([-1 15.25 37.5 44.5])
xlabel('Lon')
ylabel('Lat')
title('MFS-4a3 SSH field interpolated on SAT grid')
set(gca, 'Fontsize',14, 'Color',[0.5 0.5 0.5])
exportfile = ['/mnt/data2/sciascia/ConnectivitySimulations/InputData/MFS_v4a3_map',yrstr,'.png'];
print(fig3,'-dpng', exportfile)

fig4=figure(4);
set(gcf,'Position', [141 147 1083 558],'InvertHardcopy','off','PaperPositionMode','auto')
pcolor(SATlon,SATlat,SATssh_2004); shading interp; caxis([EtaMin EtaMax]); colorbar;colormap(bluewhitered)
axis([-1 15.25 37.5 44.5])
xlabel('Lon')
ylabel('Lat')
title('SAT SSH field')
set(gca, 'Fontsize',14, 'Color',[0.5 0.5 0.5])
exportfile = ['/mnt/data2/sciascia/ConnectivitySimulations/InputData/SAT_map',yrstr,'.png'];
print(fig4,'-dpng', exportfile)

fig8=figure(8);
set(gcf,'Position', [141 147 1083 558],'InvertHardcopy','off','PaperPositionMode','auto')
pcolor(ROMSlon,ROMSlat,ROMSssh_2004); shading interp; caxis([EtaMin EtaMax]); colorbar;colormap(bluewhitered)
axis([-1 15.25 37.5 44.5])
xlabel('Lon')
ylabel('Lat')
title('ROMS SSH field')
set(gca, 'Fontsize',14, 'Color',[0.5 0.5 0.5])
exportfile = ['/mnt/data2/sciascia/ConnectivitySimulations/InputData/ROMS_map',yrstr,'.png'];
print(fig8,'-dpng', exportfile)



sqdiff=diff.*diff;
sqdiff_old=diff_old.*diff_old;

sqr_diff_2004=sqrt(nanmean(sqdiff,3));
sqr_diffold_2004=sqrt(nanmean(sqdiff_old,3));


sqr_diff_series=sqrt(squeeze(nanmean(nanmean(sqdiff,1),2)));
sqr_diffold_series=sqrt(squeeze(nanmean(nanmean(sqdiff_old,1),2)));


fig5=figure(5);
set(gcf,'Position', [141 147 1083 558],'InvertHardcopy','off','PaperPositionMode','auto')
pcolor(SATlon,SATlat,sqr_diff_2004*100); shading interp; 
%caxis([EtaMin EtaMax]); 
colorbar;colormap(jet)
xlabel('Lon')
ylabel('Lat')
title('SAT - MFS 4b3:  rms field [cm]')
set(gca, 'Fontsize',14, 'Color',[0.5 0.5 0.5])
exportfile = ['/mnt/data2/sciascia/ConnectivitySimulations/InputData/diff_map_v4b3',yrstr,'.png'];
print(fig5,'-dpng', exportfile)

fig6=figure(6);
set(gcf,'Position', [141 147 1083 558],'InvertHardcopy','off','PaperPositionMode','auto')
pcolor(SATlon,SATlat,sqr_diffold_2004*100); shading interp; 
%caxis([EtaMin EtaMax]); 
colorbar;colormap(jet)
xlabel('Lon')
ylabel('Lat')
title('SAT - MFS 4a3:  rms field [cm]')
set(gca, 'Fontsize',14, 'Color',[0.5 0.5 0.5])
exportfile = ['/mnt/data2/sciascia/ConnectivitySimulations/InputData/diff_map_v4a3',yrstr,'.png'];
print(fig6,'-dpng', exportfile)


fig7=figure(7);
set(gcf,'Position', [165 337 1120 387],'InvertHardcopy','on','PaperPositionMode','auto')
plot(time,sqr_diff_series*100,'r');
hold on;
plot(time,sqr_diffold_series*100,'b');
hold off
h=legend('MFS - 4b3','MFS - 4a3');
set(h,'Location','SouthEast')
set(gca,'FontSize',14)
axis([0 size(time,2) 0 20])
datetick('x','mmm')
xlabel(yrstr)
ylabel(' rms SSH [cm]')
exportfile = ['/mnt/data2/sciascia/ConnectivitySimulations/InputData/diff_series_',yrstr,'.png'];
print(fig7,'-dpng', exportfile)

