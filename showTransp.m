% Script to calculate and show transports along the Corsica Channel
% section from the 1/64 deg runs of the Automatic project for 
% the Ligurian Sea using RomsMatlab functions
%
% itimu
%

addpath(genpath('/mnt/data2/sciascia/utilities/'))
clear all; close all;
expt = 4 ;
expstr = num2str(expt);


outdir = ['/mnt/iscsi/ROMS/lig1kmAuto/Exp',expstr,'/','Analysis/'];
outname = ['CorsicaChannel_Exp',expstr,'.mat'];
outnameMFS = ['CorsicaChannel_MFS_2004.mat'];
outfile=[outdir,outname];
outfileMFS=[outdir,outnameMFS];

disp('Loading data...');
load(outfile);
load(outfileMFS);
disp('...done!!');

%%% Reduce width of the Corsica channel to be consistent with Observations
%%%Corsica channel definded only between Corsica and Capraia Island 

SHOWOBS=1;

if(SHOWOBS)

   %% ROMS

   ii=find(CorsChann.lon(:,1)>=9.8);
   dimChan=[1:ii(1,1)];

   %set 3D variables

   tmp.salt(:,:,:) = CorsChann.salt(dimChan,:,:);
   tmp.temp(:,:,:) = CorsChann.temp(dimChan,:,:);
   tmp.uvel(:,:,:) = CorsChann.uvel(dimChan,:,:);
   tmp.vvel(:,:,:) = CorsChann.vvel(dimChan,:,:);
   tmp.nvel(:,:,:) = CorsChann.nvel(dimChan,:,:);
   tmp.pvel(:,:,:) = CorsChann.pvel(dimChan,:,:);
   tmp.dens(:,:,:) = CorsChann.dens(dimChan,:,:);
   tmp.pres(:,:,:) = CorsChann.pres(dimChan,:,:);
   tmp.z(:,:,:)    = CorsChann.z(dimChan,:,:);
   tmp.dz(:,:,:)   = CorsChann.dz(dimChan,:,:);
   
   %set 2d variables at the end
   tmp.bott = CorsChann.bott(dimChan,:);
   tmp.dist = CorsChann.dist(dimChan,:);
   tmp.lon  = CorsChann.lon(dimChan,:);
   tmp.lat  = CorsChann.lat(dimChan,:);
   tmp.ang  = CorsChann.ang(dimChan,:);
   tmp.datenum= CorsChann.datenum; 
   clear CorsChann
   CorsChann=tmp;
   clear tmp ii

 
   %%%% MFS
   ii=find(CorsChanMfs.lonv(:,1)>=9.8);
   dimChan=[1:ii(1,1)];


    tmp.salt(:,:,:)=CorsChanMfs.salt(dimChan,:,:);
    tmp.temp(:,:,:)=CorsChanMfs.temp(dimChan,:,:);
    tmp.uvel(:,:,:)=CorsChanMfs.uvel(dimChan,:,:);
    tmp.vvel(:,:,:)=CorsChanMfs.vvel(dimChan,:,:);
    tmp.lonr(:,:)=CorsChanMfs.lonr(dimChan,:);
    tmp.latr(:,:)=CorsChanMfs.latr(dimChan,:);
    tmp.lonu(:,:)=CorsChanMfs.lonu(dimChan,:);
    tmp.latu(:,:)=CorsChanMfs.latu(dimChan,:);
    tmp.lonv(:,:)=CorsChanMfs.lonv(dimChan,:);
    tmp.latv(:,:)=CorsChanMfs.latv(dimChan,:);
    tmp.dz(:,:)=CorsChanMfs.dz(dimChan,:);
    tmp.dep(:,:)=CorsChanMfs.dep(dimChan,:);
    tmp.datenum=CorsChanMfs.datenum;
   
   clear CorsChanMfs
   CorsChanMfs=tmp;
   clear tmp ii


   %%%% Observations 
   outnameObs = ['daily_transp_2004.mat'];
   outfileObs= [outdir,outnameObs];
  load(outfileObs);
end


lim2004=[2:1465];

%%%% ROMS
%calculate dx from dist
dx = diff(CorsChann.dist,1,1).*1e3;
dx = cat(1,dx,dx(end,:));
dx = repmat(dx,[1 1 size(CorsChann.dz,3)]);
trans = CorsChann.nvel.*CorsChann.dz.*dx;
TotT = squeeze(sum(sum(trans,1),2)) ;

TotT_2004=mean(reshape(TotT(lim2004,:),4,366),1);

%%%% MFS
[dxM,angle] = sw_dist(CorsChanMfs.latu(:,1),CorsChanMfs.lonu(:,1),'km' );
dxM = [dxM; dxM(end)].*1e3;
dxM = repmat(dxM,[1 size(CorsChanMfs.dz,2) size(CorsChanMfs.vvel,3)]);
dzM = repmat(CorsChanMfs.dz, [1 1 size(CorsChanMfs.vvel,3)]);
transM = CorsChanMfs.vvel.*dzM.*dxM;
TotT_Mfs = squeeze(nansum(nansum(transM,1),2)) ;

firstDays = [1 125 241 365 485 609 729 853 977 1097 1221 1341]; 
refdate = datenum(2004,1,1);
firstDays = (firstDays - 1)/4 + refdate ;

dateVec = CorsChann.datenum;
dateVec_Mfs = CorsChanMfs.datenum;

% Plot

set(gcf,'Position', [255 662 1410 406],'PaperPositionMode','auto')
%hp(1)=plot(dateVec(lim2004),TotT(lim2004)/1e6,'k','linewidth',2);
hp(1)=plot(Time,TotT_2004/1e6,'k','linewidth',2);
set(gca,'xtick',firstDays);
xlim([min(dateVec(lim2004)) max(dateVec(lim2004))]);
datetick('x',19,'keepticks','keeplimits');
hold on
plot(dateVec(lim2004),TotT(lim2004)*0,'k','linewidth',1);
hp(2)=plot(dateVec_Mfs,TotT_Mfs/1e6,'Color',[.7 .7 .7],'linewidth',2);

if(SHOWOBS)
hp(3)=plot(Time,Tran,'Color',[.5 .5 .5],'LineWidth',2);
end

hYLabel=ylabel('Mass Transport [Sv]');
hXLabel=xlabel('Year 2004 [dd/mm]');
ylim([-2 2]);
hold off
set(gca,'DataAspectRatio',[25 1 1]);
legend(hp,'ROMS','MFS','OBS','location','SouthWest');

set( gca                       , ...
    'FontName'   , 'Helvetica' );
set(hXLabel, ...
    'FontName'   , 'Helvetica');
set( gca                       , ...
    'FontSize'   , 14          );
set(hXLabel , ...
    'FontSize'   , 14          );
set(hYLabel , ...
    'FontSize'   , 14          );
set(hYLabel, ...
    'FontName'   , 'Helvetica');

printname = ['OBS_Transp2004_Exp',expstr,'.eps'];
printfile= [outdir,printname];
print('-depsc2','-r300',printfile);

Tname=['Transport.mat'];
Tfile=[outdir,Tname];

save(Tfile,'TotT','TotT_Mfs','Tran','dateVec','lim2004','dateVec_Mfs','Time');


%%%%%%%%%%%%%%%%%%%
%%
% Some statistics 
%
%

if(SHOWOBS)
  MTran=nanmean(Tran);
  STran=nanstd(Tran);
end
MTotT_Mfs=mean(TotT_Mfs)/1e6;
STotT_Mfs=std(TotT_Mfs)/1e6;
MTotT=mean(TotT(lim2004))/1e6;
STotT=std(TotT(lim2004))/1e6;


