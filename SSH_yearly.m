% script for yearly average of SSH 


addpath(genpath('/mnt/data2/sciascia/utilities/'))

clear all; close all;



% Read MFS


yr = 2004 ;
yrstr = num2str(yr);

% Read MFS v4a3

for i=1:12
    if i<10
       mth=['0',num2str(i)];
    else
       mth=num2str(i);
    end

    MFSdir = '/mnt/data2/ROMS/mfs/Ssh/';
    sname = ['Mfs_Ssh_',mth,'_',yrstr,'.nc'];
    sshFile = [MFSdir,sname];
    ssh_temp=ncread(sshFile,'sossheig');
    if i==1
       MFSssh=ssh_temp;
    else
      MFSssh=cat(3,MFSssh,ssh_temp);
    end
end

Mlat=double(ncread(sshFile,'lat'));
Mlon=double(ncread(sshFile,'lon'));
[MFSlon,MFSlat]=meshgrid(Mlon,Mlat);

%avrg_MFSssh=nanmean(MFSssh(:));
%MFSssh=MFSssh-avrg_MFSssh;

MFSssh_2004=squeeze(nanmean(MFSssh,3))';

% Read SATELLITE SLA 


SATdir = '/mnt/data2/ROMS/SatData/CopernicusSatData/MergedSat/';
sname = ['AllSat_Merged_MeanSeaLevel_L4_REP_OBSERVATIONS_',yrstr,'.nc'];
slaFile = [SATdir,sname];
Slat=double(ncread(slaFile,'lat'));
Slon=double(ncread(slaFile,'lon'));
Slon=rem((Slon+180),360)-180;

[SATlon,SATlat]=meshgrid(Slon,Slat);

sla_temp=ncread(slaFile,'sla');
SATsla=sla_temp;



% Read SATELLITE MDT

MDTdir = '/mnt/data2/ROMS/SatData/AvisoSatData/MeanDynTopo/';
mname = ['SMDT-MED-2014-REF20.nc'];
mdtFile = [MDTdir,mname];
Mlat=double(ncread(mdtFile,'lat'));
Mlon=double(ncread(mdtFile,'lon'));
Mlon=rem((Mlon+180),360)-180;
[MDTlon,MDTlat]=meshgrid(Mlon,Mlat);

mdt=ncread(mdtFile,'mdt')';


seaSAT = ~isnan(SATsla(:,:,1)');
seaMDT = ~isnan(mdt(:,:,1));

mdtInt = NaN*SATlon;

mdtInt(seaSAT) = griddata(MDTlon(seaMDT),MDTlat(seaMDT),mdt(seaMDT),SATlon(seaSAT),SATlat(seaSAT),'nearest');

SATsla_avrg=squeeze(nanmean(SATsla,3));
SATssh_2004=SATsla_avrg'+mdtInt;

% Read Satellite ADT (Absolute Dynamic Topography)

ADTdir = '/mnt/data2/ROMS/SatData/AvisoSatData/MergedSat/AbsDynTopo/';
aname = ['Aviso_AllSat_Merged_AbsDynTopo_DT_2004.nc'];
adtFile = [ADTdir,aname];
Alat=double(ncread(adtFile,'lat'));
Alon=double(ncread(adtFile,'lon'));
Alon=rem((Alon+180),360)-180;
[ADTlon,ADTlat]=meshgrid(Alon,Alat);

adt=ncread(adtFile,'adt');

SATadt_2004=squeeze(nanmean(adt,3))';

% ROMS MODEL 

nt=1465;

for i = 1:nt
    if i>=1000, blank = ''; end
    if i<1000, blank = '0'; end
    if i<100,  blank = '00'; end
    if i<10,   blank = '000'; end
    sname = ['/mnt/iscsi/ROMS/lig1kmAuto/Exp4/Out_data/ocean_his_',blank,num2str(i),'.nc'];

    sshFile = [sname];
    ROMSlat=ncread(sshFile,'lat_rho');
    ROMSlon=ncread(sshFile,'lon_rho');
    ssh_temp=squeeze(ncread(sshFile,'zeta'));
    ROMSssh(:,:,i)=ssh_temp;

end

%ROMS_mssh=nanmean(nanmean(nanmean(ROMSssh)));
%ROMSssh=ROMSssh-ROMS_mssh;


ROMSssh_2004=squeeze(nanmean(ROMSssh,3));


EtaMin=-0.25;
EtaMax=0.25;


fig1=figure(1);
set(gcf,'Position', [141 147 1083 558],'InvertHardcopy','off','PaperPositionMode','auto')
pcolor(MFSlon,MFSlat,MFSssh_2004); shading interp;
caxis([EtaMin EtaMax]); colorbar;colormap(bluewhitered)
axis([-1 15.25 37.5 44.5])
xlabel('Lon')
ylabel('Lat')
set(gca, 'Fontsize',14, 'Color',[0.5 0.5 0.5])
exportfile = ['/mnt/data2/sciascia/ConnectivitySimulations/InputData/MFS_2004.png'];
print(fig1,'-dpng', exportfile)

fig2=figure(2);
set(gcf,'Position', [141 147 1083 558],'InvertHardcopy','off','PaperPositionMode','auto')
pcolor(SATlon,SATlat,SATssh_2004); shading interp;
caxis([EtaMin EtaMax]); colorbar;colormap(bluewhitered)
axis([-1 15.25 37.5 44.5])
xlabel('Lon')
ylabel('Lat')
set(gca, 'Fontsize',14, 'Color',[0.5 0.5 0.5])
exportfile = ['/mnt/data2/sciascia/ConnectivitySimulations/InputData/SATssh_2004.png'];
print(fig2,'-dpng', exportfile)

fig3=figure(3);
set(gcf,'Position', [141 147 1083 558],'InvertHardcopy','off','PaperPositionMode','auto')
pcolor(ADTlon,ADTlat,SATadt_2004); shading interp;
caxis([EtaMin EtaMax]); colorbar;colormap(bluewhitered)
axis([-1 15.25 37.5 44.5])
xlabel('Lon')
ylabel('Lat')
set(gca, 'Fontsize',14, 'Color',[0.5 0.5 0.5])
exportfile = ['/mnt/data2/sciascia/ConnectivitySimulations/InputData/SATadt_2004.png'];
print(fig3,'-dpng', exportfile)

fig4=figure(4);
set(gcf,'Position', [141 147 1083 558],'InvertHardcopy','off','PaperPositionMode','auto')
pcolor(ROMSlon,ROMSlat,ROMSssh_2004); shading interp;
caxis([EtaMin EtaMax]); colorbar;colormap(bluewhitered)
axis([-1 15.25 37.5 44.5])
xlabel('Lon')
ylabel('Lat')
set(gca, 'Fontsize',14, 'Color',[0.5 0.5 0.5])
exportfile = ['/mnt/data2/sciascia/ConnectivitySimulations/InputData/ROMS_2004.png'];
print(fig4,'-dpng', exportfile)
