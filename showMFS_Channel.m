%
%Script to plot monthly averages of temperature and salinity in the corsica channel 
% for different MFS years
% RS
%


addpath(genpath('/mnt/data2/sciascia/utilities/'))
addpath(genpath('~/Corsica_Marcello/'))
clear all; close all;

%define month
mth='Nov';

% Read 2004

yr1 = 2004 ;
yrstr1 = num2str(yr1);

msg=['read', yrstr1];
disp(msg)



MFSdir = '/mnt/data2/ROMS/mfs/Analysis/';
mfsname = ['CorsicaChannel_MFS_',mth,'04.mat'];
MFSFile = [MFSdir,mfsname];
fld1=load(MFSFile);


% Read 2006

yr2 = 2006 ;
yrstr2 = num2str(yr2);

msg=['read', yrstr2];
disp(msg)



MFSdir = '/mnt/data2/ROMS/mfs/Analysis/';
mfsname = ['CorsicaChannel_MFS_',mth,'06.mat'];
MFSFile = [MFSdir,mfsname];
fld2=load(MFSFile);



mdep=[1:37];


tmin=min(min(min(fld1.CorsChanMfs.temp(:,mdep))),min(min(fld2.CorsChanMfs.temp(:,mdep))));
tmax=max(max(max(fld1.CorsChanMfs.temp(:,mdep))),max(max(fld2.CorsChanMfs.temp(:,mdep))));

smin=min(min(min(fld1.CorsChanMfs.salt(:,mdep))),min(min(fld2.CorsChanMfs.salt(:,mdep))));
smax=max(max(max(fld1.CorsChanMfs.salt(:,mdep))),max(max(fld2.CorsChanMfs.salt(:,mdep))));


fig1=figure(1);

set(gcf,'Position', [656 430 998 671],'PaperPositionMode','auto')

set(gcf,'Units','points')
set(gcf,'PaperUnits','points') 

subplot(2,1,1)


set(gca, 'FontSize',14)
pcolor(fld1.CorsChanMfs.lonr(:,mdep), -fld1.CorsChanMfs.dep(:,mdep),fld1.CorsChanMfs.temp(:,mdep)); shading flat;

caxis([tmin tmax])

title(['Temperature ',mth, ' - top ',yrstr1, ' bottom ', yrstr2],'FontSize',14)

subplot(2,1,2)
pcolor(fld2.CorsChanMfs.lonr(:,mdep), -fld2.CorsChanMfs.dep(:,mdep),fld2.CorsChanMfs.temp(:,mdep)); shading flat;
caxis ([tmin tmax])
set(gca, 'Fontsize',14)

h=colorbar;
set(h,'position',[0.92 0.02 0.04 0.94])

xlabel('Longitude','Fontsize',14)

[ax1,h1]=suplabel('Latitude','y');
set(h1,'Fontsize',14)
figureTemp=['/mnt/data2/ROMS/mfs/Analysis/CorChan_temp_',mth,'_',yrstr1,'_',yrstr2,'.png'];
print(fig1,'-dpng','-r300',figureTemp)




fig2=figure(2);

set(gcf,'Position', [656 430 998 671],'PaperPositionMode','auto')

set(gcf,'Units','points')
set(gcf,'PaperUnits','points')

subplot(2,1,1)


set(gca, 'FontSize',14)
pcolor(fld1.CorsChanMfs.lonr(:,mdep), -fld1.CorsChanMfs.dep(:,mdep),fld1.CorsChanMfs.salt(:,mdep)); shading flat;

caxis([smin smax])

title(['Salinity ',mth, ' - top ',yrstr1, ' bottom ', yrstr2],'FontSize',14)

subplot(2,1,2)
pcolor(fld2.CorsChanMfs.lonr(:,mdep), -fld2.CorsChanMfs.dep(:,mdep),fld2.CorsChanMfs.salt(:,mdep)); shading flat;
caxis ([smin smax])
set(gca, 'Fontsize',14)

h=colorbar;
set(h,'position',[0.92 0.02 0.04 0.94])

xlabel('Longitude','Fontsize',14)

[ax1,h1]=suplabel('Latitude','y');
set(h1,'Fontsize',14)
figureSalt=['/mnt/data2/ROMS/mfs/Analysis/CorChan_salt_',mth,'_',yrstr1,'_',yrstr2,'.png'];
print(fig2,'-dpng','-r300',figureSalt)
