% Script to retrieve all variables along  ROMS Nice Calvi 
% section from the 1/64 deg runs of the Automatic project for 
% the Ligurian Sea using RomsMatlab functions
%
% RS
%

addpath(genpath('/mnt/data2/sciascia/utilities/'))
addpath(genpath('~/Corsica_Marcello/'))

clear all; close all;

%update data appending at the end
APPEND = 0 ;

% Startup
fprintf(1,' Script to retrieve fields at different times along the Nice Calvi section from the 1/64 deg Automatic Ligurian Sea model.\n\n') ;

domain = 'lig1kmAuto';
refdate = datenum(2004,1,1);

%Section definition
disp('Nice Calvi');
lonSec = [+8.077 +9.314] ; 
latSec = [+43.779 +42.965] ;     

%experiment definition
expt = 4 ;
expstr = num2str(expt);

outdir = ['/mnt/iscsi/ROMS/lig1kmAuto/Exp',expstr,'/','Analysis'];
outname = ['/NiceCalvi_Exp',expstr,'.mat'];
outfile=[outdir,outname];

if (APPEND)
    load(outfile)
end

%time limits
istr = 1;
iend = 1465;

inname = 'Out_data';
indir = ['/mnt/iscsi/ROMS/lig1kmAuto/Exp',expstr,'/',inname];

%Isave = 720; %for exp0 exp1
Isave = 360; %for exp2 exp3 exp4
rho0 = 1e3;

g = 9.801;

%set 3D variables

for it = istr:iend
    i_str = num2str((it-1)*Isave);
    innum = ['0000',num2str(it)];
    innum = innum(end-3:end);
    inname = ['/ocean_his_',innum,'.nc'];
    fileIn = [indir,inname];
    S = getSectFld(fileIn,lonSec,latSec);

    CorsChann.salt(:,:,it) = S.salt ;
    CorsChann.temp(:,:,it) = S.temp ;
    CorsChann.uvel(:,:,it) = S.uvel ;
    CorsChann.vvel(:,:,it) = S.vvel ;
    CorsChann.nvel(:,:,it) = S.nvel ;
    CorsChann.pvel(:,:,it) = S.pvel ;
    CorsChann.dens(:,:,it) = S.dens ;
    CorsChann.pres(:,:,it) = S.pres ;
    CorsChann.z(:,:,it)    = S.z    ;
    CorsChann.dz(:,:,it)   = S.dz   ;
    CorsChann.datenum(it)  = S.time + refdate;
end %for it

%set 2d variables at the end
CorsChann.bott = S.bott ;
CorsChann.dist = S.dist ;
CorsChann.lon  = S.lon  ;
CorsChann.lat  = S.lat  ;
CorsChann.ang  = S.ang  ;

if (APPEND)
    disp('Updating...');
else
    disp('Writing out...');
end
save(outfile,'CorsChann');
disp('...done!!!');


