%
%Script to plot hydrographic sections [T,S] 
%of the nizza Calvi section
%
% RS
%

addpath(genpath('/mnt/data2/sciascia/utilities/'))
addpath(genpath('~/Corsica_Marcello/'))
clear all; close all;

%experiment definition
expt = 4 ;
expstr = num2str(expt);

% read lig1km
disp('read ROMS data...')

ROMSname = 'Analysis';
ROMSdir = ['/mnt/iscsi/ROMS/lig1kmAuto/Exp',expstr,'/',ROMSname];
inname = ['/CorsicaChannel_Exp',expstr,'.mat'];
Filein = [ROMSdir,inname];
load(Filein)

% Read MFS
disp('read MFS data...')

mfsname = ['/CorsicaChannel_MFS_2004.mat'];
MFSFile = [ROMSdir,mfsname];
load(MFSFile)

% set dates
refdate = datenum(2004,1,1);

% day to plot
datestr='24092004';
plotday=datenum(2004,9,24);
doy=plotday-refdate+1;

% set limits 
lim2004=[2:1465];


% In case you want to plot the corsica channel 

%  set depth range for the MFS corsica channel 
mdep=[1:37];

% set yearly variables 
temp_2004=CorsChann.temp(:,:,lim2004);
salt_2004=CorsChann.salt(:,:,lim2004);
z_2004=CorsChann.z(:,:,lim2004);

temp_daily=squeeze(mean(reshape(temp_2004, 43,50,4, 366),3));
salt_daily=squeeze(mean(reshape(salt_2004,43,50,4, 366),3));
z_daily=squeeze(mean(reshape(z_2004,43,50,4, 366),3));

t=squeeze(temp_daily(:,:,doy));
salt=squeeze(salt_daily(:,:,doy));
z=squeeze(z_daily(:,:,doy));

tmin=min(min(min(t(:,:))),min(min(CorsChanMfs.temp(:,mdep,doy))));
tmax=max(max(max(t(:,:))),max(max(CorsChanMfs.temp(:,mdep,doy))));

smin=min(min(min(salt(:,:))),min(min(CorsChanMfs.salt(:,mdep,doy))));
smax=max(max(max(salt(:,:))),max(max(CorsChanMfs.salt(:,mdep,doy))));

LonMoor=ones(50,1)*9.688;

fg1=figure(1);

set(fg1,'Color',[1 1 1],'InvertHardCopy','off','Position', [188 650 1159 412],'PaperPositionMode','auto')

pcolor(CorsChann.lon,z,t); shading interp;
caxis([tmin tmax])
colorbar
hold on 
plot(LonMoor,z(7,:)*1,'-k','LineWidth',1.5);
hold off
xlabel('Lon','FontSize',14,'FontName','Helvetica')
ylabel('Depth [m]', 'FontSize',14,'FontName','Helvetica')
set(gca, 'FontSize',12,'FontName','Helvetica','Color',[.6 .6 .6])

fg1name=['/TCorsChann_',datestr,'.png'];
fg1file=[ROMSdir,fg1name];
print(fg1,'-dpng','-r300',fg1file)


fg2=figure(2);

set(fg2,'Color',[1 1 1],'InvertHardCopy','off','Position', [188 650 1159 412],'PaperPositionMode','auto')

pcolor(CorsChann.lon,z,salt); shading interp;
caxis([smin smax])
colorbar
hold on 
plot(LonMoor,z(7,:)*1,'-k','LineWidth',1.5);
hold off
xlabel('Lon','FontSize',14,'FontName','Helvetica')
ylabel('Depth [m]', 'FontSize',14,'FontName','Helvetica')
set(gca, 'FontSize',12,'FontName','Helvetica','Color',[.6 .6 .6])

fg2name=['/SCorsChann_',datestr,'.png'];
fg2file=[ROMSdir,fg2name];
print(fg2,'-dpng','-r300',fg2file)


fg3=figure(3);

set(fg3,'InvertHardCopy','off','Color',[1 1 1],'Position', [188 650 1159 412],'PaperPositionMode','auto')
pcolor(CorsChanMfs.lonr(:,mdep), -CorsChanMfs.dep(:,mdep),CorsChanMfs.temp(:,mdep,doy)); shading interp;
caxis([tmin tmax])
colorbar
xlabel('Lon','FontSize',14,'FontName','Helvetica')
ylabel('Depth [m]', 'FontSize',14,'FontName','Helvetica')
set(gca, 'FontSize',12,'FontName','Helvetica','Color',[.6 .6 .6])

fg3name=['/TCorsChann_MFS_',datestr,'.png'];
fg3file=[ROMSdir,fg3name];
print(fg3,'-dpng','-r300',fg3file)


fg4=figure(4);

set(fg4,'InvertHardCopy','off','Color',[1 1 1],'Position', [188 650 1159 412],'PaperPositionMode','auto')
pcolor(CorsChanMfs.lonr(:,mdep), -CorsChanMfs.dep(:,mdep),CorsChanMfs.salt(:,mdep,doy)); shading interp;
caxis([smin smax])
colorbar
xlabel('Lon','FontSize',14,'FontName','Helvetica')
ylabel('Depth [m]', 'FontSize',14,'FontName','Helvetica')
set(gca, 'FontSize',12,'FontName','Helvetica','Color',[.6 .6 .6])

fg4name=['/SCorsChann_MFS_',datestr,'.png'];
fg4file=[ROMSdir,fg4name];
print(fg4,'-dpng','-r300',fg4file)

