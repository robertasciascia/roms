% Script to retrieve all variables along a ROMS  Corsica Channel
% section from the 1/64 deg runs of the Automatic project for 
% the Ligurian Sea using RomsMatlab functions
% from monthly mean fields 
%
% RS
%
addpath(genpath('/mnt/data2/sciascia/utilities/'))
addpath(genpath('~/Corsica_Marcello/'))
clear all; close all;


% Startup
fprintf(1,' Script to retrieve monthly fields along the Corsica Channel section from the 1/64 deg Automatic Ligurian Sea model.\n\n') ;

domain = 'lig1kmAuto';
refdate = datenum(2004,1,1);

%Section definition
disp('Corsica Channel');
%lonSec = [+9.55 +10.45] ;
lonSec = [+9.45 +9.8] ; % New section between Corsica and Capraia Island 
latSec = [+43 +43] ;

%experiment definition
expt = 4 ;
expstr = num2str(expt);
mth='Dec';


inname = 'Analysis';
indir = ['/mnt/iscsi/ROMS/lig1kmAuto/Exp',expstr,'/',inname];
Filename = ['/ocean_avg_',mth,'_2004.nc'];
fileIn = [indir,Filename];

% output file 
outname='Analysis';
outfile = ['/mnt/iscsi/ROMS/lig1kmAuto/Exp',expstr,'/',outname,'/CorsicaChannel_',mth,'04.mat'];

% set some constants 

rho0 = 1e3;
g = 9.801;

%set 3D variables

    S = getSectFld(fileIn,lonSec,latSec);

    CorsChann.salt(:,:) = S.salt ;
    CorsChann.temp(:,:) = S.temp ;
    CorsChann.uvel(:,:) = S.uvel ;
    CorsChann.vvel(:,:) = S.vvel ;
    CorsChann.nvel(:,:) = S.nvel ;
    CorsChann.pvel(:,:) = S.pvel ;
    CorsChann.dens(:,:) = S.dens ;
    CorsChann.pres(:,:) = S.pres ;
    CorsChann.z(:,:)    = S.z    ;
    CorsChann.dz(:,:)   = S.dz   ;

%set 2d variables at the end
CorsChann.bott = S.bott ;
CorsChann.dist = S.dist ;
CorsChann.lon  = S.lon  ;
CorsChann.lat  = S.lat  ;
CorsChann.ang  = S.ang  ;

save(outfile,'CorsChann');
disp('...done!!!');
