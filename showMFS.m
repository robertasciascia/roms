% script to compare SST and SSS for different MFS years 

addpath(genpath('/mnt/data2/sciascia/utilities/'))

clear all; close all;

% Read MFS

%define month
mth='Dec';



yr1 = 2004 ;
yrstr1 = num2str(yr1);

msg=['read', yrstr1];
disp(msg)

MFSdir = '/mnt/data2/ROMS/mfs/';
tname = ['Tem/Mfs_Tem_',mth,'_',yrstr1,'.nc'];
tempFile = [MFSdir,tname];
temp1=ncread(tempFile,'votemper');
lat_MFS=ncread(tempFile,'lat');
lon_MFS=ncread(tempFile,'lon');
sname = ['Sal/Mfs_Sal_',mth,'_',yrstr1,'.nc'];
saltFile = [MFSdir,sname];
salt1=ncread(saltFile,'vosaline');

yr2= 2006 ;

yrstr2 = num2str(yr2);
msg=['read', yrstr2];
disp(msg)

MFSdir = '/mnt/data2/ROMS/mfs/';
tname = ['Tem/Mfs_Tem_',mth,'_',yrstr2,'.nc'];
tempFile = [MFSdir,tname];
temp2=ncread(tempFile,'votemper');
sname = ['Sal/Mfs_Sal_',mth,'_',yrstr2,'.nc'];
saltFile = [MFSdir,sname];
salt2=ncread(saltFile,'vosaline');

% plot data
% limit of lat and lon for MFS to have a grid similar to ROMS

mlat=[118:230];
mlon=[80:295];



tmin=min(min(min(temp1(mlon,mlat,1))),min(min(temp2(mlon,mlat,1))));
tmax=max(max(max(temp1(mlon,mlat,1))),max(max(temp2(mlon,mlat,1))));

smin=min(min(min(salt1(mlon,mlat,1))),min(min(salt2(mlon,mlat,1))));
smax=max(max(max(salt1(mlon,mlat,1))),max(max(salt2(mlon,mlat,1))));


fig1=figure(1);

set(gcf,'Position', [903 437 897 664],'PaperPositionMode','auto')


subplot(2,1,1)

set(gca, 'FontSize',14)
pcolor(lon_MFS(mlon), lat_MFS(mlat), temp1(mlon,mlat,1)'); shading flat
caxis([tmin tmax])
title(['SST ',mth,' - top ',  yrstr1, ' bottom ', yrstr2],'FontSize',14)
subplot(2,1,2)
pcolor(lon_MFS(mlon), lat_MFS(mlat), temp2(mlon,mlat,1)'); shading flat
caxis ([tmin tmax])
set(gca, 'Fontsize',14)

h=colorbar;
set(h,'position',[0.92 0.03 0.05 0.94])

xlabel('Longitude','Fontsize',14)

[ax1,h1]=suplabel('Latitude','y');
set(h1,'Fontsize',14)

figureSST=['/mnt/data2/ROMS/mfs/Analysis/SST',mth,'_',yrstr1,'_',yrstr2,'.png'];
print(fig1,'-dpng','-r300',figureSST)




fig2=figure(2);

set(gcf,'Position', [903 437 897 664],'PaperPositionMode','auto')


subplot(2,1,1)

set(gca, 'FontSize',14)
pcolor(lon_MFS(mlon), lat_MFS(mlat), salt1(mlon,mlat,1)'); shading flat
caxis([smin smax])
title(['SSS ',mth,' - top ',  yrstr1, ' bottom ', yrstr2],'FontSize',14)
subplot(2,1,2)
pcolor(lon_MFS(mlon), lat_MFS(mlat), salt2(mlon,mlat,1)'); shading flat
caxis ([smin smax])
set(gca, 'Fontsize',14)

h=colorbar;
set(h,'position',[0.92 0.03 0.05 0.94])

xlabel('Longitude','Fontsize',14)

[ax1,h1]=suplabel('Latitude','y');
set(h1,'Fontsize',14)

figureSSS=['/mnt/data2/ROMS/mfs/Analysis/SSS',mth,'_',yrstr1,'_',yrstr2,'.png'];
print(fig2,'-dpng','-r300',figureSSS)

