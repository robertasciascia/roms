% script to compute differences in space and time between SAT and ROMS 


addpath(genpath('/mnt/data2/sciascia/utilities/'))

clear all; close all;


yr = 2004 ;
yrstr = num2str(yr);

yr1 = 2005 ;
yrstr1 = num2str(yr1);

% Read SATELLITE SLA 2004


SATdir = '/mnt/data2/ROMS/SatData/CopernicusSatData/MergedSat/';
sname = ['AllSat_Merged_MeanSeaLevel_L4_REP_OBSERVATIONS_',yrstr,'.nc'];
slaFile = [SATdir,sname];
Slat=double(ncread(slaFile,'lat'));
Slon=double(ncread(slaFile,'lon'));
Slon=rem((Slon+180),360)-180;

[SATlon,SATlat]=meshgrid(Slon,Slat);

sla_temp=ncread(slaFile,'sla');
SATsla=sla_temp;


SATdir = '/mnt/data2/ROMS/SatData/CopernicusSatData/MergedSat/';
sname1 = ['AllSat_Merged_MeanSeaLevel_L4_REP_OBSERVATIONS_',yrstr1,'.nc'];
slaFile1 = [SATdir,sname1];

sla_temp1=ncread(slaFile1,'sla');
SATsla1=sla_temp1;



% Read SATELLITE MDT

MDTdir = '/mnt/data2/ROMS/SatData/AvisoSatData/MeanDynTopo/';
mname = ['SMDT-MED-2014-REF20.nc'];
mdtFile = [MDTdir,mname];
Mlat=double(ncread(mdtFile,'lat'));
Mlon=double(ncread(mdtFile,'lon'));
Mlon=rem((Mlon+180),360)-180;
[MDTlon,MDTlat]=meshgrid(Mlon,Mlat);

mdt=ncread(mdtFile,'mdt')';


seaSAT = ~isnan(SATsla(:,:,1)');
seaMDT = ~isnan(mdt(:,:,1));

mdtInt = NaN*SATlon;
mdtInt(seaSAT) = griddata(MDTlon(seaMDT),MDTlat(seaMDT),mdt(seaMDT),SATlon(seaSAT),SATlat(seaSAT),'nearest');


for i=1:size(SATsla,3)
SATssh(:,:,i)=squeeze(SATsla(:,:,i))'+mdtInt;
end

for i=1:size(SATsla1,3)
SATssh1(:,:,i)=squeeze(SATsla1(:,:,i))'+mdtInt;
end

SATlon_int=SATlon(61:116,40:169);
SATlat_int=SATlat(61:116,40:169);

SATssh_map=squeeze(nanmean(SATssh(61:116,40:169,:),3));
SATssh_2004=squeeze(nanmean(nanmean(SATssh(61:116,40:169,:),1),2));

SATssh_map_2005=squeeze(nanmean(SATssh1(61:116,40:169,:),3));
SATssh_2005=squeeze(nanmean(nanmean(SATssh1(61:116,40:169,:),1),2));


% Read Satellite ADT (Absolute Dynamic Topography)

ADTdir = '/mnt/data2/ROMS/SatData/AvisoSatData/MergedSat/AbsDynTopo/';
aname = ['Aviso_AllSat_Merged_AbsDynTopo_DT_2004.nc'];
adtFile = [ADTdir,aname];
Alat=double(ncread(adtFile,'lat'));
Alon=double(ncread(adtFile,'lon'));
Alon=rem((Alon+180),360)-180;
[ADTlon,ADTlat]=meshgrid(Alon,Alat);

adt=ncread(adtFile,'adt');

SATadt_map=squeeze(nanmean(adt,3))';
SATadt_2004=squeeze(nanmean(nanmean(adt,1),2));



% Read MFS v4a3

for i=1:12
    if i<10
       mth=['0',num2str(i)];
    else
       mth=num2str(i);
    end

    MFSdir = '/mnt/data2/ROMS/mfs/Ssh/';
    sname = ['Mfs_Ssh_',mth,'_',yrstr,'.nc'];
    sshFile = [MFSdir,sname];
    ssh_temp=ncread(sshFile,'sossheig');
    sname1 = ['Mfs_Ssh_',mth,'_',yrstr1,'.nc'];
    sshFile1 = [MFSdir,sname1];
    ssh_temp1=ncread(sshFile1,'sossheig');
    if i==1
       MFSssh=ssh_temp;
       MFSssh1=ssh_temp1;
    else
      MFSssh=cat(3,MFSssh,ssh_temp);
      MFSssh1=cat(3,MFSssh1,ssh_temp1);
    end
end

Mlat=double(ncread(sshFile,'lat'));
Mlon=double(ncread(sshFile,'lon'));
[MFSlon,MFSlat]=meshgrid(Mlon,Mlat);


MFSssh_map=squeeze(nanmean(MFSssh(81:341,118:230,:),3))';
MFSssh_2004=squeeze(nanmean(nanmean(MFSssh(81:341,118:230,:),1),2));
MFSssh_map_2005=squeeze(nanmean(MFSssh1(81:341,118:230,:),3))';
MFSssh_2005=squeeze(nanmean(nanmean(MFSssh1(81:341,118:230,:),1),2));
MFSlon_int=MFSlon(118:230,81:341);
MFSlat_int=MFSlat(118:230,81:341);






% ROMS MODEL 

nt=1465;

for i = 1:nt
    if i>=1000, blank = ''; end
    if i<1000, blank = '0'; end
    if i<100,  blank = '00'; end
    if i<10,   blank = '000'; end
    sname = ['/mnt/iscsi/ROMS/lig1kmAuto/Exp4/Out_data/ocean_his_',blank,num2str(i),'.nc'];
    sname1 = ['/mnt/iscsi/ROMS/lig1kmAuto/Exp4/Out_data/ocean_his_',blank,num2str(i),'.nc'];
    sshFile = [sname];
    sshFile1 = [sname1];
    ROMSlat=ncread(sshFile,'lat_rho');
    ROMSlon=ncread(sshFile,'lon_rho');
    ssh_temp=squeeze(ncread(sshFile,'zeta'));
    ROMSssh(:,:,i)=ssh_temp;

    ssh_temp1=squeeze(ncread(sshFile1,'zeta'));
    ROMSssh1(:,:,i)=ssh_temp1;
end

ROMSssh_map=squeeze(nanmean(ROMSssh,3));
ROMSssh_avrg=squeeze(nanmean(nanmean(ROMSssh,1),2));
ROMSssh_2004=nanmean(reshape(ROMSssh_avrg(1:1464,:),4,[]),1)';

ROMSssh_map_2005=squeeze(nanmean(ROMSssh1,3));
ROMSssh_avrg_2005=squeeze(nanmean(nanmean(ROMSssh1,1),2));
ROMSssh_2005=nanmean(reshape(ROMSssh_avrg_2005(1:1464,:),4,[]),1)';
%%%% Interp %%%%%

seaSAT = ~isnan(SATssh_map(:,:));
seaROMS = ~isnan(ROMSssh(:,:,1));

ROMSInt = NaN*SATssh_map;
ROMSInt(seaSAT) = griddata(ROMSlon(seaROMS),ROMSlat(seaROMS),ROMSssh_map(seaROMS),SATlon_int(seaSAT),SATlat_int(seaSAT),'nearest');


seaMFS = ~isnan(MFSssh_map(:,:));

MFSInt = NaN*MFSssh_map;
MFSInt(seaMFS) = griddata(MFSlon(seaMFS),MFSlat(seaMFS),MFSssh_map(seaMFS),SATlon_int(seaSAT),SATlat_int(seaSAT),'nearest');


%%%%% Figure %%%%%%

EtaMin=-0.2;
EtaMax=0.2;



diff_2004=SATssh_2004(1:366,1)-ROMSssh_2004;
diff_2004_MFS=SATssh_2004(1:366,1)-MFSssh_2004;
diff2004_mean=mean(diff_2004);
diff_map=SATssh_map-ROMSInt;
diff_map_MFS=SATssh_map-MFSInt;
diffmap_mean=nanmean(diff_map(:));

fig1=figure(1);
set(gcf,'Position', [141 147 1083 558],'InvertHardcopy','off','PaperPositionMode','auto')
pcolor(SATlon_int,SATlat_int,diff_map); shading interp;
colorbar;
%colormap(bluewhitered)
%caxis([EtaMin EtaMax])
xlabel('Lon')
ylabel('Lat')
set(gca, 'Fontsize',14, 'Color',[0.5 0.5 0.5])
exportfile = ['/mnt/data2/sciascia/ConnectivitySimulations/InputData/diff_map.png'];
print(fig1,'-dpng', exportfile)



fig2=figure(2);

set(gcf,'Position', [165 337 1120 387],'InvertHardcopy','on','PaperPositionMode','auto')
plot(SATssh_2004,'r','LineWidth',1.5);hold on;
plot(ROMSssh_2004,'k','LineWidth',1.5);
plot(MFSssh_2004,'b','LineWidth',1.5);
hold off
h=legend('SAT','ROMS','MFS');
set(h,'Location','SouthEast')
set(gca,'FontSize',14)
xlabel(2004)
ylabel('SSH [m]')
exportfile = ['/mnt/data2/sciascia/ConnectivitySimulations/InputData/SSH_SATROMS',yrstr,'.png'];
print(fig2,'-dpng', exportfile)


fig6=figure(6);

set(gcf,'Position', [165 337 1120 387],'InvertHardcopy','on','PaperPositionMode','auto')
plot(SATssh_2005,'r','LineWidth',1.5);hold on;
plot(ROMSssh_2005,'k','LineWidth',1.5);
plot(MFSssh_2005,'b','LineWidth',1.5);
hold off
h=legend('SAT','ROMS','MFS');
set(h,'Location','SouthEast')
set(gca,'FontSize',14)
xlabel(2004)
ylabel('SSH [m]')
exportfile = ['/mnt/data2/sciascia/ConnectivitySimulations/InputData/SSH_SATROMS',yrstr1,'.png'];
print(fig6,'-dpng', exportfile)



fig3=figure(3);

set(gcf,'Position', [165 337 1120 387],'InvertHardcopy','on','PaperPositionMode','auto')
plot(diff_2004,'k','LineWidth',1.5);
set(h,'Location','SouthEast')
set(gca,'FontSize',14)
xlabel(2004)
ylabel('SSH_{SAT} - SAT_{ROMS} [m]')
exportfile = ['/mnt/data2/sciascia/ConnectivitySimulations/InputData/Diff_',yrstr,'.png'];
print(fig3,'-dpng', exportfile)


fig4=figure(4);
set(gcf,'Position', [141 147 1083 558],'InvertHardcopy','off','PaperPositionMode','auto')
pcolor(ROMSlon,ROMSlat,(ROMSssh_map+0.11)); shading interp;
caxis([EtaMin EtaMax])
colorbar;colormap(bluewhitered)
xlabel('Lon')
ylabel('Lat')
set(gca, 'Fontsize',14, 'Color',[0.5 0.5 0.5])
exportfile = ['/mnt/data2/sciascia/ConnectivitySimulations/InputData/ROMSssh_modified.png'];
print(fig4,'-dpng', exportfile)


fig5=figure(5);
set(gcf,'Position', [141 147 1083 558],'InvertHardcopy','off','PaperPositionMode','auto')
pcolor(SATlon_int,SATlat_int,SATssh_map); shading interp;
caxis([EtaMin EtaMax])
colorbar;colormap(bluewhitered)
xlabel('Lon')
ylabel('Lat')
set(gca, 'Fontsize',14, 'Color',[0.5 0.5 0.5])
exportfile = ['/mnt/data2/sciascia/ConnectivitySimulations/InputData/SATssh_modified.png'];
print(fig5,'-dpng', exportfile)
