% script to plot monthly averages of SST and SSS from 
% MFS and ROMS lig1km

addpath(genpath('/mnt/data2/sciascia/utilities/'))

clear all; close all;

%experiment definition
expt = 3 ;
expstr = num2str(expt);

%define month
mth='Dec';
% read lig1km
disp('read ROMS data...')

ROMSname = 'Out_data';
ROMSdir = ['/mnt/data2/ROMS/lig1kmAuto/Exp',expstr,'/',ROMSname];
inname = ['/ocean_avg_',mth,'_2004.nc'];
Filein = [ROMSdir,inname];
temp_ROMS=ncread(Filein,'temp');
lat_ROMS=ncread(Filein,'lat_rho');
lon_ROMS=ncread(Filein,'lon_rho');
salt_ROMS=ncread(Filein,'salt');

% Read MFS
disp('read MFS data...')

MFSdir = '/mnt/data2/ROMS/mfs/';
tname = ['Tem/Mfs_Tem_',mth,'_2004.nc'];
tempFile = [MFSdir,tname];
temp_MFS=ncread(tempFile,'votemper');
lat_MFS=ncread(tempFile,'lat');
lon_MFS=ncread(tempFile,'lon');
sname = ['Sal/Mfs_Sal_',mth,'_2004.nc'];
saltFile = [MFSdir,sname];
salt_MFS=ncread(saltFile,'vosaline');

% plot data
% limit of lat and lon for MFS to have a grid similar to ROMS
% and ROMS limit to exclude adriatic 
mlat=[118:230];
mlon=[80:295];
rlon=[1:500];


tmin=min(min(min(temp_ROMS(rlon,:,50))),min(min(temp_MFS(mlon,mlat,1))));
tmax=max(max(max(temp_ROMS(rlon,:,50))),max(max(temp_MFS(mlon,mlat,1))));

smin=min(min(min(salt_ROMS(rlon,:,50))),min(min(salt_MFS(mlon,mlat,1))));
smax=max(max(max(salt_ROMS(rlon,:,50))),max(max(salt_MFS(mlon,mlat,1))));

fig1=figure(1);

set(gcf,'Position', [903 437 897 664],'PaperPositionMode','auto')


subplot(2,1,1)


set(gca, 'FontSize',14)
pcolor(lon_ROMS(rlon,:), lat_ROMS(rlon,:),temp_ROMS(rlon,:,50)); shading flat; 

caxis([tmin tmax])

title(['SST ',mth,'2004 - top ROMS, bottom MFS'],'FontSize',14)
subplot(2,1,2)
pcolor(lon_MFS(mlon), lat_MFS(mlat), temp_MFS(mlon,mlat,1)'); shading flat
caxis ([tmin tmax])
set(gca, 'Fontsize',14)

h=colorbar;
set(h,'position',[0.92 0.03 0.05 0.94])

xlabel('Longitude','Fontsize',14)

[ax1,h1]=suplabel('Latitude','y');
set(h1,'Fontsize',14)


figureSST=['/mnt/data2/ROMS/lig1kmAuto/Exp',expstr,'/Analysis/SST_',mth,'_2004.png'];
print(fig1,'-dpng','-r300',figureSST)


fig2=figure(2);

set(gcf,'Position', [903 437 897 664],'PaperPositionMode','auto')


subplot(2,1,1)


set(gca, 'FontSize',14)
pcolor(lon_ROMS(rlon,:), lat_ROMS(rlon,:),salt_ROMS(rlon,:,50)); shading flat; 

caxis([smin smax])

title(['SSS ',mth,'2004 - top ROMS, bottom MFS'],'FontSize',14)

subplot(2,1,2)
pcolor(lon_MFS(mlon), lat_MFS(mlat), salt_MFS(mlon,mlat,1)'); shading flat
caxis ([smin smax])
set(gca, 'Fontsize',14)

h=colorbar;
set(h,'position',[0.92 0.03 0.05 0.92])

xlabel('Longitude','Fontsize',14)

[ax1,h1]=suplabel('Latitude','y');
set(h1,'Fontsize',14)

figureSSS=['/mnt/data2/ROMS/lig1kmAuto/Exp',expstr,'/Analysis/SSS_',mth,'_2004.png'];
print(fig2,'-dpng','-r300',figureSSS)

