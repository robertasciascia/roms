%
%
%Script to plot MFS transport in the Corsica Channel for differen years
%
% RS
%

addpath(genpath('/mnt/data2/sciascia/utilities/'))
addpath(genpath('~/Corsica_Marcello/'))
clear all; close all;

yr1 = 2004 ;
yrstr1 = num2str(yr1);


yr2 = 2006 ;
yrstr2 = num2str(yr2);

outfile2004 = ['/mnt/data2/ROMS/mfs/Analysis/CorsicaChannel_MFS_2004.mat'];
outfile2006 = ['/mnt/data2/ROMS/mfs/Analysis/CorsicaChannel_MFS_2006.mat'];

disp('Loading data 2004...');
fld1=load(outfile2004);

disp('Loading data 2006...');
fld2=load(outfile2006);
disp('...done!!');


%%%% MFS 2004
[dxM,angle] = sw_dist(fld1.CorsChanMfs.latu(:,1),fld1.CorsChanMfs.lonu(:,1),'km' );
dxM = [dxM; dxM(end)].*1e3;
dxM = repmat(dxM,[1 size(fld1.CorsChanMfs.dz,2) size(fld1.CorsChanMfs.vvel,3)]);
dzM = repmat(fld1.CorsChanMfs.dz, [1 1 size(fld1.CorsChanMfs.vvel,3)]);
transM = fld1.CorsChanMfs.vvel.*dzM.*dxM;
TotT_2004 = squeeze(nansum(nansum(transM,1),2));

%%%% MFS 2006
[dxM,angle] = sw_dist(fld2.CorsChanMfs.latu(:,1),fld2.CorsChanMfs.lonu(:,1),'km' );
dxM = [dxM; dxM(end)].*1e3;
dxM = repmat(dxM,[1 size(fld2.CorsChanMfs.dz,2) size(fld2.CorsChanMfs.vvel,3)]);
dzM = repmat(fld2.CorsChanMfs.dz, [1 1 size(fld2.CorsChanMfs.vvel,3)]);
transM = fld2.CorsChanMfs.vvel.*dzM.*dxM;
TotT_2006 = squeeze(nansum(nansum(transM,1),2)) ;


fig1=figure(1);

set(gcf,'Position', [572 597 1333 450],'PaperPositionMode','auto')

set(gca, 'FontSize',14)

plot(TotT_2004/1e6,'b','linewidth',2);

hold on; 


plot(TotT_2006/1e6,'r','linewidth',2);

hold off;

k=legend('2004','2006','Orientation', 'Vertical');
set(k, 'FontSize',12, 'Location', 'SouthEast')
xlabel('Day of the year','FontSize',14)
ylabel('Transport [Sv]','FontSize',14) 

figureTransp=['/mnt/data2/ROMS/mfs/Analysis/Transport_',yrstr1,'_',yrstr2,'.png'];
print(fig1,'-dpng','-r300',figureTransp)
