%
%Script to plot velocities  in the corsica channel 
%
% RS
%

addpath(genpath('/mnt/data2/sciascia/utilities/'))
addpath(genpath('~/Corsica_Marcello/'))
clear all; close all;

%experiment definition
expt = 4 ;
expstr = num2str(expt);
ANNMEAN=0;
% read lig1km
disp('read ROMS data...')

ROMSname = 'Analysis';
ROMSdir = ['/mnt/iscsi/ROMS/lig1kmAuto/Exp',expstr,'/',ROMSname];
inname = ['/CorsicaChannel_Exp',expstr,'.mat'];
Filein = [ROMSdir,inname];
load(Filein)

% Read MFS
disp('read MFS data...')

mfsname = ['/CorsicaChannel_MFS_2004.mat'];
MFSFile = [ROMSdir,mfsname];
load(MFSFile)

% set dates
refdate = datenum(2004,1,1);

% day to plot
datestr='13052004';
plotday=datenum(2004,5,13);
doy=plotday-refdate+1; 



% set limits 
lim2004=[2:1465];
% depth range for the MFS corsica channel 
mdep=[1:37];
% Longitude mooring
LonMoor=ones(50,1)*9.688;


% set yearly variables 
v_2004=CorsChann.vvel(:,:,lim2004);
z_2004=CorsChann.z(:,:,lim2004);
dens_2004=CorsChann.dens(:,:,lim2004);

v_daily=squeeze(mean(reshape(v_2004, 43,50,4, 366),3));
z_daily=squeeze(mean(reshape(z_2004,43,50,4, 366),3));
dens_daily=squeeze(mean(reshape(dens_2004,43,50,4, 366),3));

v=squeeze(v_daily(:,:,doy));
z=squeeze(z_daily(:,:,doy));
dens=squeeze(dens_daily(:,:,doy));

vmin=min(min(min(v(:,:))),min(min(CorsChanMfs.vvel(:,mdep,doy))));
vmax=max(max(max(v(:,:))),max(max(CorsChanMfs.vvel(:,mdep,doy))));

% yearly mean 

v_yr=squeeze(mean(v_daily(:,:,:),3));
z_yr=squeeze(mean(z_daily(:,:,:),3));
dens_yr=squeeze(mean(dens_daily(:,:,:),3))+1000;

v_yrmin=min(min(min(v_yr(:,:))),min(min(mean(CorsChanMfs.vvel(:,mdep,:),3))));
v_yrmax=max(max(max(v_yr(:,:))),max(max(mean(CorsChanMfs.vvel(:,mdep,:),3))));


fg1=figure(1);

set(fg1,'Color',[1 1 1],'InvertHardCopy','off','Position', [188 650 1159 412],'PaperPositionMode','auto')

pcolor(CorsChann.lon,z,v); shading interp;
caxis([vmin vmax])
colormap(bluewhitered)
colorbar
hold on 
[C,h]=contour(CorsChann.lon,z,dens,'k');
%v = [1028.4, 1028.6 1028.8 1029];
clabel(C,h,'FontName','Helvetica','FontSize',12,'FontWeight','bold')
plot(LonMoor,z(7,:)*1,'-k','LineWidth',1.5);
hold off
xlabel('Lon','FontSize',14,'FontName','Helvetica')
ylabel('Depth [m]', 'FontSize',14,'FontName','Helvetica')
set(gca, 'FontSize',12,'FontName','Helvetica','Color',[.6 .6 .6])

fg1name=['/VCorsChann_',datestr,'.png'];
fg1file=[ROMSdir,fg1name];
print(fg1,'-dpng','-r300',fg1file)

fg2=figure(2);

set(fg2,'InvertHardCopy','off','Color',[1 1 1],'Position', [188 650 1159 412],'PaperPositionMode','auto')

pcolor(CorsChanMfs.lonr(:,mdep), -CorsChanMfs.dep(:,mdep),CorsChanMfs.vvel(:,mdep,doy)); shading interp;

caxis([vmin vmax])
colormap(bluewhitered)
colorbar
xlabel('Lon','FontSize',14,'FontName','Helvetica')
ylabel('Depth [m]', 'FontSize',14,'FontName','Helvetica')
set(gca, 'FontSize',12,'FontName','Helvetica','Color',[.6 .6 .6])

fg2name=['/VCorsChann_MFS_',datestr,'.png'];
fg2file=[ROMSdir,fg2name];
print(fg2,'-dpng','-r300',fg2file)


if(ANNMEAN)

fg3=figure(3);

set(fg3,'Color',[1 1 1],'InvertHardCopy','off','Position', [188 650 1159 412],'PaperPositionMode','auto')

pcolor(CorsChann.lon,z,v_yr); shading interp;
caxis([v_yrmin v_yrmax])
colormap(bluewhitered)
colorbar
hold on
[C,h]=contour(CorsChann.lon,z,dens_yr,'k');
v = [1028.4, 1028.6 1028.8 1029];
clabel(C,h,v,'FontName','Helvetica','FontSize',12,'FontWeight','bold')
plot(LonMoor,z(7,:)*1,'-k','LineWidth',1.5);
hold off
xlabel('Lon','FontSize',14,'FontName','Helvetica')
ylabel('Depth [m]', 'FontSize',14,'FontName','Helvetica')
set(gca, 'FontSize',12,'FontName','Helvetica','Color',[.6 .6 .6])

fg3name=['/VCorsChann_2004.png'];
fg3file=[ROMSdir,fg3name];
print(fg3,'-dpng','-r300',fg3file)

fg4=figure(4);

set(fg4,'InvertHardCopy','off','Color',[1 1 1],'Position', [188 650 1159 412],'PaperPositionMode','auto')

pcolor(CorsChanMfs.lonr(:,mdep), -CorsChanMfs.dep(:,mdep),squeeze(mean(CorsChanMfs.vvel(:,mdep,:),3))); shading interp;
caxis([v_yrmin v_yrmax])
colormap(bluewhitered)
colorbar
xlabel('Lon','FontSize',14,'FontName','Helvetica')
ylabel('Depth [m]', 'FontSize',14,'FontName','Helvetica')
set(gca, 'FontSize',12,'FontName','Helvetica','Color',[.6 .6 .6])

fg4name=['/VCorsChann_MFS_2004.png'];
fg4file=[ROMSdir,fg4name];
print(fg4,'-dpng','-r300',fg4file)

end
