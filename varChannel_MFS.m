% script to retrieve all variables along the Corsica Channel
% section from the 1/16 deg runs of MFS
% using RomsMatlab functions
% using monthly mean 
% RS
%
addpath(genpath('/mnt/data2/sciascia/utilities/'))
addpath(genpath('~/Corsica_Marcello/'))
clear all; close all;

% Startup
fprintf(1,' Script to retrieve monthly fields along the Corsica Channel section from the 1/16deg MFS model. \n\n') ;

MFSpath = '/mnt/data2/ROMS/mfs/';
posfix = '.nc';
mth='Dec';
yy=2006;
yystr=num2str(yy);
    disp('  Salinity...')
    mfssalfile = [MFSpath,'Sal/Mfs_Sal_',mth,'_',yystr,posfix];
    D = getMfsCorChanFld(mfssalfile,'vosaline');
    CorsChanMfs.salt(:,:) = D.var;
    CorsChanMfs.lonr = D.lon;
    CorsChanMfs.latr = D.lat;
    CorsChanMfs.dz = D.dz;
    CorsChanMfs.dep = D.dep;
    clear D
    disp('  Temperature...')
    mfstemfile =[MFSpath,'Tem/Mfs_Tem_',mth,'_',yystr,posfix];
    D = getMfsCorChanFld(mfstemfile,'votemper');
    CorsChanMfs.temp(:,:) = D.var;
    clear D
    disp('  U-Velocity...')
    mfsuvlfile = [MFSpath,'Uvl/Mfs_Uvl_',mth,'_',yystr,posfix];
    D = getMfsCorChanFld(mfsuvlfile,'vozocrtx');
    CorsChanMfs.uvel(:,:) = D.var;
    CorsChanMfs.lonu = D.lonu;
    CorsChanMfs.latu = D.latu;
    clear D
    disp('  V-Velocity...')
    mfsvvlfile =[MFSpath,'Vvl/Mfs_Vvl_',mth,'_',yystr,posfix];
    D = getMfsCorChanFld(mfsvvlfile,'vomecrty');
    CorsChanMfs.vvel(:,:) = D.var;
    CorsChanMfs.lonv = D.lonv;
    CorsChanMfs.latv = D.latv;
    clear D


disp(' ')
disp('-------------------------------')
outfile = ['/mnt/data2/ROMS/mfs/Analysis/CorsicaChannel_MFS_',mth,'06.mat'];

disp(' ')
disp('Writing out...');
save(outfile,'CorsChanMfs');
disp('...done!!!');

