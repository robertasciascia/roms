%
%Script to plot velocities  in the corsica channel 
%
% RS
%

addpath(genpath('/mnt/data2/sciascia/utilities/'))
addpath(genpath('~/Corsica_Marcello/'))
clear all; close all;

%experiment definition
expt = 4 ;
expstr = num2str(expt);
ANNMEAN=0;
% read lig1km
disp('read ROMS data...')

ROMSname = 'Analysis';
ROMSdir = ['/mnt/iscsi/ROMS/lig1kmAuto/Exp',expstr,'/',ROMSname];
inname = ['/CorsicaChannel_Exp',expstr,'.mat'];
Filein = [ROMSdir,inname];
load(Filein)

% Read MFS
disp('read MFS data...')

mfsname = ['/CorsicaChannel_MFS_2004.mat'];
MFSFile = [ROMSdir,mfsname];
load(MFSFile)

% set dates
refdate = datenum(2004,1,1);

% set limits 
lim2004=[2:1465];
% depth range for the MFS corsica channel 
mdep=[1:37];
% Indices for the plot

% Longitude mooring (9.68 ROMS index=7; MFS index=4)
% Depth (50m ROMS index=26; MFS index=12)
% Depth (100m ROMS index=17; MFS index=18)
% Depth (300m ROMS index=4; MFS, index= 30)
% Depth (380m ROMS index=1; MFS index= 33)

Lindx=7;
Lindx_MFS=4;

dpth=380;
sdpth=num2str(dpth);

if(dpth==50)
   dindx=26;
   dindx_MFS=12;
elseif(dpth==100)
   dindx=17;
   dindx_MFS=18;
elseif(dpth==300)
   dindx=4;
   dindx_MFS=30;
else
   dindx=1;
   dindx_MFS=33;
end


% set yearly variables 
v_2004=CorsChann.vvel(:,:,lim2004);
u_2004=CorsChann.uvel(:,:,lim2004);
salt_2004=CorsChann.salt(:,:,lim2004);

v_daily=squeeze(mean(reshape(v_2004, 43,50,4, 366),3));
u_daily=squeeze(mean(reshape(u_2004, 43,50,4, 366),3));
salt_daily=squeeze(mean(reshape(salt_2004, 43,50,4,366),3));

dtime=[1:366];


figure(1)
set(1,'Position', [5 57 1014 632],'PaperPositionMode','auto');
set(gca,'Fontname','Helvetica','FontSize',12);
scale=0;
for ii=1:1:dtime(1,end)
 hold on
 plot(dtime(ii),0,'k-')
 quiver(dtime(ii),0,squeeze(u_daily(Lindx,dindx,ii))*100,squeeze(v_daily(Lindx,dindx,ii))*100,scale,'b.')
end
quiver(310,100,20,0,0,'.b')
text(310,110,'20 cm/s')
hold off;
axis equal;
set(gca,'yticklabel','');
set(gca,'Xgrid','on')
xlabel('2004 [d]','FontSize',14);
ylabel('Current Velocity ','FontSize',14);
box on;
printname = ['/Figure/SP_CorsicaChannel_',sdpth,'m.png'];
printfile= [ROMSdir,printname];
print('-dpng','-r300',printfile);

figure(2)

set(2,'Position', [5 57 1014 632],'PaperPositionMode','auto');
set(gca,'Fontname','Helvetica','FontSize',12);
scale=0;
for ii=1:1:dtime(1,end)
 hold on
 plot(dtime(ii),0,'k-')
 quiver(dtime(ii),0,squeeze(CorsChanMfs.uvel(Lindx_MFS,dindx_MFS,ii))*100,squeeze(CorsChanMfs.vvel(Lindx_MFS,dindx_MFS,ii))*100,scale,'b.')
end
quiver(310,100,20,0,0,'.b')
text(310,110,'20 cm/s')
hold off;
axis equal;
set(gca,'yticklabel','');
set(gca,'Xgrid','on')
xlabel('2004 [d]','FontSize',14);
ylabel('Current Velocity ','FontSize',14);
box on;
printname = ['/Figure/SP_CorsicaChannelMFS_',sdpth,'m.png'];
printfile= [ROMSdir,printname];
print('-dpng','-r300',printfile);

% Plot to show correlation 
% in this case I use the bottom index
dindx=1;
dindx_MFS=33;

% file with ERAInterim Wind average West of Corsica
load('/mnt/iscsi/ROMS/lig1kmAuto/Exp4/Analysis/UWindCC_2004.mat');

fg3=figure(3);
set(fg3,'Position', [163 52 845 653],'PaperPositionMode','auto')
set(gca,'FontSize',12,'FontName','Helvetica')

subplot(3,1,1)
plot(dtime, u_avrgCC(:,1),'-k','LineWidth',1.5);

ylabel('EW Wind [cm/s]','FontSize',14)
xlabel('2004','FontSize',14)
box off
datetick('x','dd/mm')

subplot(3,1,2)
plot(dtime, squeeze(v_daily(Lindx,dindx,:))*100,'-k','LineWidth',1.5);

ylabel('NS Velocity [cm/s]','FontSize',14)
xlabel('2004','FontSize',14)
box off
datetick('x','dd/mm')

subplot(3,1,3)

plot(dtime, squeeze(salt_daily(Lindx,dindx,:)),'-k','LineWidth',1.5);
ylabel('Salinity','FontSize',14)
box off
datetick('x','dd/mm')

